/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ejb.ClientTable;
import ejb.ClientTableFacade;
import ejb.ModelTableFacade;
import ejb.ReservationTable;
import ejb.ReservationTableFacade;
import ejb.UserTable;
import ejb.UserTableFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import util.Controller;

/**
 *
 * @author jleci
 */
public class HomeServlet extends HttpServlet {
    @EJB
    private ReservationTableFacade reservationTableFacade;
    @EJB
    private ClientTableFacade clientTableFacade1;

    @EJB
    private UserTableFacade userFacade;
    @EJB
    private ClientTableFacade clientTableFacade;
    @EJB
    private ModelTableFacade modelTableFacade;

    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
     try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")==null){
                String site = "login";
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", site);
                return;
            }
             List<String> modelList = modelTableFacade.getTypesByModelForChart();
               String model_grouping_chart = "var data = [";
               String[] splitedS = null;
               for(String s : modelList){
                   splitedS = s.split(",");
                   model_grouping_chart += "{label: \""+splitedS[1]+"\", data: "+splitedS[0]+"},";
               }
               
               model_grouping_chart = model_grouping_chart.substring(0 , model_grouping_chart.length() - 1) + "];";
               request.setAttribute("model_gruping_chart", model_grouping_chart);
               
               System.out.println(modelList);
             
               
            Date today = new Date(System.currentTimeMillis());
            List<ClientTable> ownerList = clientTableFacade.status();
            request.setAttribute("ownerList", ownerList);
            
             List<ReservationTable> reservations = reservationTableFacade.findAll();
             request.setAttribute("reservations", reservations);
             
             
            
            List<ReservationTable> canceledReservations=reservationTableFacade.canceledRequests();
            request.setAttribute("canceledReservations", canceledReservations);
            
             List<ReservationTable> acceptedRequests = reservationTableFacade.acceptedRequests();
            request.setAttribute("acceptedRequests", acceptedRequests);
            
           
            
            request.getRequestDispatcher("index.jsp").include(request, response);
            
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
