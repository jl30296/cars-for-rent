<%@page import="ejb.ReservationTable"%>
<%@page import="ejb.GroupTable"%>
<%@page import="ejb.ClientTable"%>
<%@page import="ejb.UserTable"%>
<%@page import="ejb.TypeTable"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>

<%
    String model_gruping_chart = (String) request.getAttribute("model_gruping_chart");

    System.out.println(model_gruping_chart);
%>
<html lang="en" class="no-js">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <link rel="shortcut icon" href="template/assets/img/icon.png"/>
        <title>Cars for Rent</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <%@include file="incl/css.html" %>

    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <div class="header navbar navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->

                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->

                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right">



                    <li class="dropdown user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <%
                                String surname = (String) session.getAttribute("surname");
                                String name = (String) session.getAttribute("name");

                                int group = (Integer) session.getAttribute("groupId");
                                out.println(name + " " + surname);

                            %>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">

                            <li>
                                <a href="/Cars-war/login.jsp">
                                    <i class="fa fa-key"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler hidden-phone">
                            </div>
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        </li>

                        <li>
                            <a href="home">
                                <i class="fa fa-home"></i>
                                <span class="title">
                                    Home
                                </span>
                                <span class="selected">
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-user"></i>
                                <span class="title">
                                    Users
                                </span>
                                <span class="arrow ">
                                </span>
                            </a>
                            <ul class="sub-menu">

                                <li>
                                    <a href="javascript:loadContent('user','listUserView');">
                                        <i class="fa fa-list-ul"></i>
                                        List Users
                                    </a>
                                </li>
                                <%if (group == 5) {%> 
                                <li style="display: block">
                                    <a href="javascript: loadContent('user', 'addUserView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add User
                                    </a>
                                </li>

                                <%} else {%>
                                <li style="display: none">
                                    <a href="javascript: loadContent('user', 'addUserView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add User
                                    </a>
                                </li>
                                <%}%>



                            </ul>

                        </li>





                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-users"></i>
                                <span class="title">
                                    Groups
                                </span>
                                <span class="arrow ">
                                </span>
                            </a>
                            <ul class="sub-menu">

                                <li>
                                    <a href="javascript:loadContent('group','listGroupView');">
                                        <i class="fa fa-list-ul"></i>
                                        List Groups
                                    </a>
                                </li>
                                <%if (group == 5) {%>
                                <li style="display: block">
                                    <a href="javascript: loadContent('group', 'addGroupView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add Group
                                    </a>
                                </li>
                                <%} else {%>
                                <li style="display: none">
                                    <a href="javascript: loadContent('group', 'addGroupView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add Group
                                    </a>
                                </li>
                                <%}%>
                            </ul>

                        </li>



                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-folder"></i>
                                <span class="title">
                                    Model
                                </span>
                                <span class="arrow ">
                                </span>
                            </a>
                            <ul class="sub-menu">

                                <li>
                                    <a href="javascript: loadContent('model', 'listModelView');">
                                        <i class="fa fa-list-ul"></i>
                                        List Model
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: loadContent('model', 'addModelView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add Model
                                    </a>
                                </li>


                            </ul>

                        </li>









                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-folder"></i>
                                <span class="title">
                                    Type
                                </span>
                                <span class="arrow ">
                                </span>
                            </a>
                            <ul class="sub-menu">


                                <li>
                                    <a href="javascript: loadContent('type', 'listTypeView');">
                                        <i class="fa fa-list-ul"></i>
                                        List Type
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript: loadContent('type', 'addTypeView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add Type
                                    </a>
                                </li>

                            </ul>

                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-truck"></i>
                                <span class="title">
                                    Vehicle
                                </span>
                                <span class="arrow ">
                                </span>
                            </a>
                            <ul class="sub-menu">

                                <li>
                                    <a href="javascript:loadContent('vehicle','listVehicleView');">
                                        <i class="fa fa-list-ul"></i>
                                        List Vehicle
                                    </a>
                                </li>
                                <%if (group == 5) {%> 
                                <li style="display: block">
                                    <a href="javascript: loadContent('vehicle', 'addVehicleView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add Vehicle
                                    </a>
                                </li>

                                <%} else {%>
                                <li style="display: none">
                                    <a href="javascript: loadContent('vehicle', 'addVehicleView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add User
                                    </a>
                                </li>
                                <%}%>


                            </ul>

                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-calendar"></i>
                                <span class="title">
                                    Reservation
                                </span>
                                <span class="arrow ">
                                </span>
                            </a>
                            <ul class="sub-menu">

                                <li>
                                    <a href="javascript: loadContent('ReservationsServlet', 'addCalendarView');">
                                        <i class="fa fa-calendar"></i>
                                        Reservaions Calendar
                                    </a>
                                </li>
                                <%
                                    if (group == 5) {
                                %>
                                <li>
                                    <a href="javascript: loadContent('ReservationsServlet', 'addReservationsRequestsView');">
                                        <i class="fa fa-list-ul"></i>
                                        Reservaions Requests
                                    </a>
                                </li>

                                <%
                                    }
                                %>

                            </ul>

                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="fa fa-user"></i>
                                <span class="title">
                                    Client
                                </span>
                                <span class="arrow ">
                                </span>
                            </a>
                            <ul class="sub-menu">

                                <li>
                                    <a href="javascript: loadContent('ClientServlet', 'clientListView');">
                                        <i class="fa fa-list-ul"></i>
                                        Cients List
                                    </a>
                                </li>

                                <li>
                                    <a href="javascript: loadContent('ClientServlet', 'addClientView');">
                                        <i class="fa fa-plus-square"></i>
                                        Add Client
                                    </a>
                                </li>




                            </ul>

                        </li>


                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <a data-toggle="modal" href="#alert_modal" id="alert_modal_trigger"></a>
                    <div class="modal fade" id="alert_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title" id="alert_modal_title"></h4>
                                </div>
                                <div class="modal-body" id="alert_modal_body">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">

                        <div class="toggler-close">
                        </div>
                        <div class="theme-options">
                            <div class="theme-option theme-colors clearfix">
                                <span>
                                    THEME COLOR
                                </span>
                                <ul>
                                    <li class="color-black current color-default" data-style="default">
                                    </li>
                                    <li class="color-blue" data-style="blue">
                                    </li>
                                    <li class="color-brown" data-style="brown">
                                    </li>
                                    <li class="color-purple" data-style="purple">
                                    </li>
                                    <li class="color-grey" data-style="grey">
                                    </li>
                                    <li class="color-white color-light" data-style="light">
                                    </li>
                                </ul>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Layout
                                </span>
                                <select class="layout-option form-control input-small">
                                    <option value="fluid" selected="selected">Fluid</option>
                                    <option value="boxed">Boxed</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Header
                                </span>
                                <select class="header-option form-control input-small">
                                    <option value="fixed" selected="selected">Fixed</option>
                                    <option value="default">Default</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Sidebar
                                </span>
                                <select class="sidebar-option form-control input-small">
                                    <option value="fixed">Fixed</option>
                                    <option value="default" selected="selected">Default</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Sidebar Position
                                </span>
                                <select class="sidebar-pos-option form-control input-small">
                                    <option value="left" selected="selected">Left</option>
                                    <option value="right">Right</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Footer
                                </span>
                                <select class="footer-option form-control input-small">
                                    <option value="fixed">Fixed</option>
                                    <option value="default" selected="selected">Default</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-home
                                       "></i>
                                    <a href="home">
                                        Home
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Dashboard
                                    </a>
                                </li>
                                <li class="pull-right">
                                    <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                        <i class="fa fa-calendar"></i>
                                        <span>
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div id="content">
                      
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <%
                                            List<ClientTable> ownerList = (List) request.getAttribute("ownerList");
                                            out.println(ownerList.size());
                                        %>
                                    </div>
                                    <div class="desc">
                                        Actual Drivers
                                    </div>
                                </div>
                                <a class="more"  href="javascript: loadContent('ClientServlet', 'clientListView');">
                                    View More <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow">
                                <div class="visual">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <%
                                            List<ReservationTable> reservationList = (List) request.getAttribute("reservations");
                                            out.println(reservationList.size());
                                        %>
                                    </div>
                                    <div class="desc">
                                        Reservations
                                    </div>
                                </div>
                                <a class="more"  href="javascript: loadContent('ReservationsServlet', 'addCalendarView');">
                                    View More <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>

            

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-times"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <%
                                            List<ReservationTable> canceled = (List) request.getAttribute("canceledReservations");
                                            out.println(canceled.size());
                                        %>
                                    </div>
                                    <div class="desc">
                                        Canceled  Requests
                                    </div>
                                </div>
                                <a class="more"  href="javascript:loadContent('ReservationsServlet','addCanceledRequestsListView');">
                                    View More <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>




                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow">
                                <div class="visual">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <%
                                            List<ReservationTable> accepted = (List) request.getAttribute("acceptedRequests");
                                            out.println(accepted.size());
                                        %>
                                    </div>
                                    <div class="desc">
                                        Accepted  Requests
                                    </div>
                                </div>
                                <a class="more"  href="javascript:loadContent('ReservationsServlet','addAcceptedRequestListView');">
                                    View More <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>

                        
                        <div class="portlet box yellow">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Models
                                </div>
                                <div class="tools">
                                    <a href="#portlet-config" data-toggle="modal" class="config">
                                    </a>
                                    <a href="javascript:;" class="reload">
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <h4>Models</h4>
                                <div id="interactive2" class="chart" style="padding: 0px; position: relative;">
                                </div>
                            </div>
                        </div>

                      
                    </div>
                    <div class="clearfix">
                    </div>



                    <!-- END PORTLET-->
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2014 &copy; Cars for Rent.
    </div>
    <div class="footer-tools">
        <span class="go-top">
            <i class="fa fa-angle-up"></i>
        </span>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- END PAGE LEVEL SCRIPTS -->
<script src="template/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="template/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="template/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="template/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="template/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="template/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="template/assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="template/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<%@include file="incl/js.html" %>

<script src="template/assets/plugins/flot/jquery.flot.min.js"></script>
<script src="template/assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="template/assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="template/assets/plugins/flot/jquery.flot.stack.min.js"></script>
<script src="template/assets/plugins/flot/jquery.flot.crosshair.min.js"></script>
<script src="template/assets/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="template/assets/scripts/core/app.js"></script>
<script src="template/assets/scripts/custom/charts.js"></script>
<script>
    jQuery(document).ready(function () {
        App.init(); // initlayout and core plugins
        Index.init();
        Index.initCalendar();
        initDeviceTypeCharts();
    });
    function initDeviceTypeCharts() {

        //var data = [{label:"hehe",data:10} , {label:"hihi",data:5}];
    <%=model_gruping_chart%>


        // var series = 10;

        //console.log(data);

        // INTERACTIVE
        $.plot($("#interactive2"), data, {
            series: {
                pie: {
                    show: true
                }
            },
            grid: {
                hoverable: true,
                clickable: true
            }
        });
        $("#interactive2").bind("plothover", pieHover);
        $("#interactive2").bind("plotclick", pieClick);

        function pieHover(event, pos, obj) {
            if (!obj)
                return;
            percent = parseFloat(obj.series.percent).toFixed(2);
            $("#hover").html('<span style="font-weight: bold; color: ' + obj.series.color + '">' + obj.series.label + ' (' + percent + '%)</span>');
        }

        function pieClick(event, pos, obj) {
            var nr = obj.series.data[0][1];
            if (!obj)
                return;
            percent = parseFloat(obj.series.percent).toFixed(2);
            var html = '<div class="form-group"><label class="col-md-3 control-label">Model Name</label><label class="col-md-4 control-label" >: ' + obj.series.label + '</label></div><br>';
            html += '<div class="form-group"><label class="col-md-3 control-label">Model Types</label><label class="col-md-4 control-label " >: ' + nr + ' types</label></div><br>';
            alertt(html);
        }

    }
    function showChartTooltip(x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff',
        }).appendTo("body").fadeIn(200);
    }




</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>