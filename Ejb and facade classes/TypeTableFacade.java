/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Vlera
 */
@Stateless
public class TypeTableFacade extends AbstractFacade<TypeTable> {

    @PersistenceContext(unitName = "Project-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TypeTableFacade() {
        super(TypeTable.class);
    }

    public boolean addType(TypeTable type) {
        try {
            create(type);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<TypeTable> listTypes() {
        Query q = em.createQuery("SELECT t FROM TypeTable t");
        return q.getResultList();
    }

    public boolean deleteType(TypeTable type) {
        try {
            remove(type);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean editType(TypeTable type) {
        try {
            edit(type);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
