/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Vlera
 */
@Stateless
public class GroupTableFacade extends AbstractFacade<GroupTable> {
    @PersistenceContext(unitName = "Project-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GroupTableFacade() {
        super(GroupTable.class);
    }
    public boolean addGroup(GroupTable group){
        try{
            create(group);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public List<GroupTable> listGroups(){
        Query q=em.createQuery("SELECT g FROM GroupTable g");
        return q.getResultList();
    }
    public boolean editGroup(GroupTable group){
        try{
            edit(group);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public boolean deleteGroup(GroupTable group){
        try{
            remove(group);
            return true;
        }catch(Exception e){
            return false;
        }
    }
   
    
}
