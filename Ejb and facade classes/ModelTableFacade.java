/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Vlera
 */
@Stateless
public class ModelTableFacade extends AbstractFacade<ModelTable> {

    @PersistenceContext(unitName = "Project-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModelTableFacade() {
        super(ModelTable.class);
    }

    public boolean addModel(ModelTable model) {
        try {
            create(model);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<ModelTable> listModels() {
        Query q = em.createQuery("SELECT m FROM ModelTable m");
        return q.getResultList();
    }

    public boolean deleteModel(ModelTable model) {
        try {
            remove(model);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean editModel(ModelTable model) {
        try {
            edit(model);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
