/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "car_table")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CarTable.findAll", query = "SELECT c FROM CarTable c"),
    @NamedQuery(name = "CarTable.findById", query = "SELECT c FROM CarTable c WHERE c.id = :id"),
    @NamedQuery(name = "CarTable.findByName", query = "SELECT c FROM CarTable c WHERE c.name = :name"),
    @NamedQuery(name = "CarTable.findByColor", query = "SELECT c FROM CarTable c WHERE c.color = :color"),
    @NamedQuery(name = "CarTable.findByRegistration", query = "SELECT c FROM CarTable c WHERE c.registration = :registration"),
    @NamedQuery(name = "CarTable.findByStatus", query = "SELECT c FROM CarTable c WHERE c.status = :status"),
    @NamedQuery(name = "CarTable.findByPrice", query = "SELECT c FROM CarTable c WHERE c.price = :price")})
public class CarTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Size(max = 45)
    @Column(name = "color")
    private String color;
    @Size(max = 45)
    @Column(name = "registration")
    private String registration;
    @Column(name = "status")
    private Short status;
    @Column(name = "price")
    private int price;
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    @ManyToOne
    private TypeTable typeId;
    @OneToMany(mappedBy = "carId")
    private List<ReservationTable> reservationTableList;

    public CarTable() {
    }

    public CarTable(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public TypeTable getTypeId() {
        return typeId;
    }

    public void setTypeId(TypeTable typeId) {
        this.typeId = typeId;
    }

    @XmlTransient
    public List<ReservationTable> getReservationTableList() {
        return reservationTableList;
    }

    public void setReservationTableList(List<ReservationTable> reservationTableList) {
        this.reservationTableList = reservationTableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarTable)) {
            return false;
        }
        CarTable other = (CarTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.CarTable[ id=" + id + " ]";
    }
    
}
