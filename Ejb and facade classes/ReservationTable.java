/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "reservation_table")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReservationTable.findAll", query = "SELECT r FROM ReservationTable r"),
    @NamedQuery(name = "ReservationTable.findById", query = "SELECT r FROM ReservationTable r WHERE r.id = :id"),
    @NamedQuery(name = "ReservationTable.findByFromDate", query = "SELECT r FROM ReservationTable r WHERE r.fromDate = :fromDate"),
    @NamedQuery(name = "ReservationTable.findByToDate", query = "SELECT r FROM ReservationTable r WHERE r.toDate = :toDate"),
    @NamedQuery(name = "ReservationTable.findByStatus", query = "SELECT r FROM ReservationTable r WHERE r.status = :status")})
public class ReservationTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "from_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Column(name = "to_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Column(name = "status")
    private Short status;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private UserTable userId;
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    @ManyToOne
    private CarTable carId;
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    @ManyToOne
    private ClientTable clientId;

    public ReservationTable() {
    }

    public ReservationTable(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public UserTable getUserId() {
        return userId;
    }

    public void setUserId(UserTable userId) {
        this.userId = userId;
    }

    public CarTable getCarId() {
        return carId;
    }

    public void setCarId(CarTable carId) {
        this.carId = carId;
    }

    public ClientTable getClientId() {
        return clientId;
    }

    public void setClientId(ClientTable clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationTable)) {
            return false;
        }
        ReservationTable other = (ReservationTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.ReservationTable[ id=" + id + " ]";
    }
    
}
