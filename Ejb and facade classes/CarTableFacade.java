/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Vlera
 */
@Stateless
public class CarTableFacade extends AbstractFacade<CarTable> {

    @PersistenceContext(unitName = "Project-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CarTableFacade() {
        super(CarTable.class);
    }

    public List<CarTable> listCars() {
        Query q = em.createQuery("SELECT c FROM CarTable c");
        return q.getResultList();
    }

    public List<TypeTable> listTypes() {
        Query q = em.createQuery("SELECT t FROM TypeTable t");
        return q.getResultList();
    }

    public boolean addCar(CarTable car) {
        try {
            create(car);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<CarTable> listVehicles() {
        Query q = em.createQuery("SELECT c FROM CarTable c");
        return q.getResultList();
    }

    public boolean editCar(CarTable car) {
        try {
            edit(car);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deleteCar(CarTable car) {
        try {
            remove(car);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
