/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Vlera
 */
@Stateless
public class UserTableFacade extends AbstractFacade<UserTable> {

    @PersistenceContext(unitName = "Project-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserTableFacade() {
        super(UserTable.class);
    }

    public UserTable login(String username, String password) {
        Query q = em.createQuery("SELECT u FROM UserTable u WHERE u.username=:username AND u.password=:password");
        q.setParameter("username", username);
        q.setParameter("password", password);
        
        return (UserTable)q.getSingleResult();
    }
   /* public UserTable usersgroup(int group_id){
        Query q=em.createQuery("SELECT u FROM UserTable u WHERE group_id=:group_id " );
        q.setParameter("group_id",group_id);
        
        return (UserTable)q.getSingleResult();
    }*/
    public boolean addUser(UserTable user){
        try{
            create(user);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    
    public List<UserTable> listUsers(){
        Query q=em.createQuery("SELECT ut FROM UserTable ut");
        return q.getResultList();
    }
    public boolean editUser(UserTable user){
        try{
            edit(user);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public boolean deleteUser(UserTable user){
        try{
            remove(user);
            return true;
        }catch(Exception e){
            return false;
        }
    }
     public boolean editUser_new(int id,String name,String surname,String password,String username){
        try{
            Query q=em.createQuery("update UserTable as u set u.name=:name,u.surname=:surname where u.id =:id");
            
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
