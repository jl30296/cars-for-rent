/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "client_table")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClientTable.findAll", query = "SELECT c FROM ClientTable c"),
    @NamedQuery(name = "ClientTable.findById", query = "SELECT c FROM ClientTable c WHERE c.id = :id"),
    @NamedQuery(name = "ClientTable.findByName", query = "SELECT c FROM ClientTable c WHERE c.name = :name"),
    @NamedQuery(name = "ClientTable.findBySurname", query = "SELECT c FROM ClientTable c WHERE c.surname = :surname"),
    @NamedQuery(name = "ClientTable.findByBirthday", query = "SELECT c FROM ClientTable c WHERE c.birthday = :birthday"),
    @NamedQuery(name = "ClientTable.findByTel", query = "SELECT c FROM ClientTable c WHERE c.tel = :tel"),
    @NamedQuery(name = "ClientTable.findByEmail", query = "SELECT c FROM ClientTable c WHERE c.email = :email"),
    @NamedQuery(name = "ClientTable.findByStatus", query = "SELECT c FROM ClientTable c WHERE c.status = :status")})
public class ClientTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Size(max = 45)
    @Column(name = "surname")
    private String surname;
    @Column(name = "birthday")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthday;
    @Column(name = "tel")
    private Integer tel;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "email")
    private String email;
    @Column(name = "status")
    private Short status;
    @OneToMany(mappedBy = "clientId")
    private List<ReservationTable> reservationTableList;

    public ClientTable() {
    }

    public ClientTable(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getTel() {
        return tel;
    }

    public void setTel(Integer tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @XmlTransient
    public List<ReservationTable> getReservationTableList() {
        return reservationTableList;
    }

    public void setReservationTableList(List<ReservationTable> reservationTableList) {
        this.reservationTableList = reservationTableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientTable)) {
            return false;
        }
        ClientTable other = (ClientTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.ClientTable[ id=" + id + " ]";
    }
    
}
