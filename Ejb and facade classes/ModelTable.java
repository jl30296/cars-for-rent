/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "model_table")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModelTable.findAll", query = "SELECT m FROM ModelTable m"),
    @NamedQuery(name = "ModelTable.findById", query = "SELECT m FROM ModelTable m WHERE m.id = :id"),
    @NamedQuery(name = "ModelTable.findByName", query = "SELECT m FROM ModelTable m WHERE m.name = :name"),
    @NamedQuery(name = "ModelTable.findByStatus", query = "SELECT m FROM ModelTable m WHERE m.status = :status")})
public class ModelTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Column(name = "status")
    private Short status;
    @OneToMany(mappedBy = "modelId")
    private List<TypeTable> typeTableList;

    public ModelTable() {
    }

    public ModelTable(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @XmlTransient
    public List<TypeTable> getTypeTableList() {
        return typeTableList;
    }

    public void setTypeTableList(List<TypeTable> typeTableList) {
        this.typeTableList = typeTableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModelTable)) {
            return false;
        }
        ModelTable other = (ModelTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.ModelTable[ id=" + id + " ]";
    }
    
}
