$(".modifyButton").click(function(){
   
    var id = $(this).get(0).id;

    var post={id:id};
   loadContent("model","editModelView",post);
});
$(".delete_button").click(function(){
   
    var id = $(this).get(0).id;

    var post={method:"deleteModel",id:id};
    $.post("model",post,function(data){
        if(data.trim()=="success"){
            bootbox.alert("Model has been deleted!");
             loadContent("model","listModelView");
        }else{
            bootbox.alert("error");
        }
    });
});
$("#submit_button").click(function(){
    var name=$("#modelname").val();
    var post={method:"editModel",name:name,globalId:globalId};
    $.post("model",post,function(data){
        if(data.trim() =="success"){
            bootbox.alert("Model has been saved!");
            loadContent("model","listModelView");
        }else{
            bootbox.alert("error");
        }
    });
   
});
