$(".modifyButton").click(function () {

    var id = $(this).get(0).id;

    var post = {id: id};
    loadContent("type", "editTypeView", post);
});
$(".delete_button").click(function () {

    var id = $(this).get(0).id;

    var post = {method: "deleteType", id: id};
    $.post("type", post, function (data) {
        if (data.trim() == "success") {
            bootbox.alert("Type has been deleted!");
            loadContent("type", "listTypeView");
        } else {
            bootbox.alert("error");
        }
    });
});
$("#submit_button").click(function () {
    var name = $("#typename").val();
    var modelId =$('#selecti').val();
    var post = {method: "editType", name: name, globalId: globalId,modelId:modelId};
   console.log(name,modelId);
    $.post("type", post, function (data) {
        if (data.trim() == "success") {
            bootbox.alert("Type has been saved!");
            loadContent("type", "listTypeView");
        } else {
            bootbox.alert("error");
        }
    });

});
$('#selecti').click(function () {
    console.log("selecti clicked");

    $('#testType').html("");
});
