$(".enable_request, .disable_request").click(function() {
    var requestId = $(this)[0].id;
    var post = {method: "changeStatus", requestId: requestId};

    $.post("reservation", post, function(data) {
        console.log(data);
        if (data.trim() != "error") {
          
           loadContent('ReservationsServlet', 'addReservationsRequestsView');
        } else {
            
        }
    });
});
$(".yes_request").click(function(){
    var yesId=$(this)[0].id;
    var post={yesId:yesId};
    loadContent("ReservationsServlet","yesRequestView",post);
});
$(".no_request").click(function(){
    var noId=$(this)[0].id;
    var post={method:"noRequest",noId:noId};
    $.post("ReservationsServlet",post,function(data){
        if(data.trim() == "success"){
            bootbox.alert("Reservation has been canceled!");
            loadContent("ReservationsServlet","addCanceledRequestsListView");
        }else{
            bootbox.alert("error");
        }
    });
});

$("#submit_button").click(function(){
    var vehicleId=$("#vehicle").val();
    var fromDate=$("#from_date").val();
    var toDate=$("#to_date").val();
    var client=$("#client").val();
    
    var post={method:"acceptRequest",client:client,vehicleId:vehicleId,reservationId:reservationId,fromDate:fromDate,toDate:toDate};
    
    $.post("ReservationsServlet",post,function(data){
        if(data.trim()=="success"){
            bootbox.alert("Reservation has been accepted!");
            loadContent("ReservationsServlet","addAcceptedRequestListView");
        }else{
            bootbox.alert(data);
        }
    });
});
