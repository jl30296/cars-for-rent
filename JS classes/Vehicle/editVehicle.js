$(".modifyButton").click(function () {

    var id = $(this).get(0).id;

    var post = {id: id};
    loadContent("vehicle", "editVehicleView", post);
});
$(".delete_button").click(function () {

    var id = $(this).get(0).id;

    var post = {method: "deleteVehicle", id: id};
    $.post("vehicle", post, function (data) {
        if (data.trim() == "success") {
            bootbox.alert("Vehicle has been deleted!");
            loadContent("vehicle", "listVehicleView");
        } else {
            bootbox.alert("error");
        }
    });
});
$("#submit_button").click(function () {
    var name=$("#vehiclename").val();
    var color=$("#vehiclecolor").val();
    var type=$("#selecti").val();
    var registration=$("#vehicleregistration").val();
    var price=$("#vehicleprice").val();
    
   var post={method:"editVehicle",name:name,color:color,type:type,registration:registration,price:price,globalId:globalId};
    console.log(type,name,color,registration);
   $.post("vehicle",post,function(data){
       if(data.trim()=="success"){
           bootbox.alert("Vehicle has been saved!");
           loadContent('vehicle','listVehicleView');
           console.log(data);
       }else{
           bootbox.alert("error");
       }
   });

});
$('#selecti').click(function () {
    console.log("selecti clicked");

    $('#testType').html("");
});
