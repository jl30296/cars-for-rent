$(".modifyButton").click(function () {

    var id = $(this).get(0).id;

    var post = {id: id};
    loadContent("user", "editUserView", post);
});
$(".delete_button").click(function () {

    var id = $(this).get(0).id;

    var post = {method: "deleteUser", id: id};
    $.post("user", post, function (data) {
        if (data.trim() == "success") {
            bootbox.alert("User has been deleted!");
            loadContent("user", "listUserView");
        } else {
            bootbox.alert("error");
        }
    });
});


$("#submit_button").click(function () {
    var name = $('#name').val();
    var surname = $('#surname').val();
    var username = $('#username').val();
    var password = $('#password').val();
    var groupId = $('#selecti').val();


    var post = {method: "editUser", name: name, globalId: globalId, surname: surname, password: password, username: username, groupId: groupId};

    console.log(groupId);
    $.post("user", post, function (data) {
        if (data.trim() == "suksese") {
            bootbox.alert("User has been saved!");
            loadContent("user", "listUserView");
        } else {
            bootbox.alert("error");
        }
    });
});
$('#selecti').click(function () {
    console.log("selecti clicked");

    $('#testType').html("");
});