$(".modifyButton").click(function(){
   
    var id = $(this).get(0).id;

    var post={id:id};
    loadContent("group","editGroupView",post);
});
$(".delete_button").click(function(){
   
    var id = $(this).get(0).id;

    var post={method:"deleteGroup",id:id};
    $.post("group",post,function(data){
        if(data.trim()=="success"){
            bootbox.alert("Group has been deleted!");
             loadContent("group","listGroupView");
        }else{
            bootbox.alert("Can't delete this!");
        }
    });
});
$("#submit_button").click(function(){
    var name=$("#groupname").val();
    var post={method:"editGroup",name:name,globalId:globalId};
    $.post("group",post,function(data){
        if(data.trim() =="success"){
            bootbox.alert("Group has been saved!");
            loadContent("group","listGroupView");
        }else{
            bootbox.alert("error");
        }
    });
});
