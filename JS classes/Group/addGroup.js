$("#submit_button").click(function () {
    var name = $("#groupname").val();
    var post = {method: "addGroup", name: name};

    $.post("group", post, function (data) {
        if (data.trim() == "success") {
            bootbox.alert("Group has been saved!");
            loadContent("group","listGroupView");
        } else {
            bootbox.alert("error");
        }
    });
});