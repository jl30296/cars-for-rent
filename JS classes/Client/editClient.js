$(".delete_button").click(function(){
 
     var id = $(this).get(0).id;

    var post={method:"deleteClient",id:id};
    $.post("ClientServlet",post,function(data){
        if(data.trim()=="success"){
              //$("#delete_group").css({"display": "block"});
               $("#delete_client").fadeIn();
           //bootbox.alert("User has been saved!");
           setTimeout(function () {
               loadContent("ClientServlet","clientListView");
            }, 800);
           
            //bootbox.alert("Group has been deleted!");
             
        }else{
            bootbox.alert("Can't delete this!");
        }
    });
});
$(".edit_button").click(function(){
    var id = $(this).get(0).id;

    var post={id:id};
    loadContent("ClientServlet","editClientView",post);
});
$("#submit_button").click(function(){
    var name = $("#client_name").val();
    var surname=$("#client_surname").val();
    var email= $("#client_email").val();
    var tel=$("#client_number").val();
    var birthday=$("#client_birthday").val();
    
    var post={method:"editClient",name:name,surname:surname,email:email,tel:tel,birthday:birthday,id:globalId};
    
    $.post("ClientServlet",post,function(data){
        if(anyFieldEmpty(name,surname,email,tel,birthday)){
         //bootbox.alert("All required!");
          //$("#add_group").css({"display": "none"});
            $("#add_client").fadeOut();
           // $("#validation").css({"display": "block"});
             $("#validation").fadeIn();
            setTimeout(function () {
              // $("#validation").css({"display": "none"});
                $("#validation").fadeOut();
            }, 1100);
        return;
    }
        
        if(data.trim() =="success"){
            //$("#edit_group").css({"display": "block"});
             $("#add_client").fadeIn();
           //bootbox.alert("User has been saved!");
           setTimeout(function () {
               loadContent("ClientServlet","clientListView");
            }, 800);
           
           // bootbox.alert("Group has been saved!");
            //loadContent("group","listGroupView");
        }
    });
});