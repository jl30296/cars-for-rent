$("#submit_button").click(function(){
    var name = $("#client_name").val();
    var surname=$("#client_surname").val();
    var email= $("#client_email").val();
    var tel=$("#client_number").val();
    var birthday=$("#client_birthday").val();
    
      if(anyFieldEmpty(name,surname,email,tel,birthday)){
         //bootbox.alert("All required!");
          //$("#add_group").css({"display": "none"});
            $("#add_client").fadeOut();
           // $("#validation").css({"display": "block"});
             $("#validation").fadeIn();
            setTimeout(function () {
              // $("#validation").css({"display": "none"});
                $("#validation").fadeOut();
            }, 1100);
        return;
    }
    
    var post={method:"addClient",name:name,surname:surname,email:email,tel:tel,birthday:birthday};
    $.post("ClientServlet",post,function(data){
        
         if (data.trim() == "success") {
          //  $("#add_group").css({"display": "block"});
            $("#add_client").fadeIn();
            //$("#validation").css({"display": "none"});
            $("#validation").fadeOut();
           // bootbox.alert("Group has been saved!");
            setTimeout(function () {
              //  loadContent("group","listGroupView");
            }, 800);      
        }
    });
    
});