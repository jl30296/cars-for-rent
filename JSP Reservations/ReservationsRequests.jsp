
<%@page import="ejb.ReservationTable"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<%@page import="java.util.List"%>
<script src="incl/Reservations/js/resevationsRequests.js" ></script>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div>
                    Requests List
                </div>
                <div class="tools">



                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="reservation_request">

                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Descriotion
                                </th>

                                <th>
                                    User
                                </th>
                                <th>
                                    From
                                </th>
                                <th>
                                    To
                                </th>
                                <th>
                                    Accept
                                </th>

                            </tr>
                        </thead>
                        <tbody>

                            <%
                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                List<ReservationTable> reservationList = (List) request.getAttribute("reservations");
                                if (reservationList != null) {
                                    int index = 0;
                                    for (ReservationTable reservations : reservationList) {
                                        Date today = new Date(System.currentTimeMillis());
                                        if (reservations.getToDate().equals(dateFormat.format(today))) {
                                            reservations.setStatus((short) 0);
                                        }

                            %>  
                            <tr>
                                <td>
                                    <%=++index%>
                                </td>
                                <td>
                                    <%=reservations.getDescription()%>
                                </td>
                                <td>
                                    <%=reservations.getUserId().getName()%>
                                </td>


                                <td>
                                    <%=dateFormat.format(reservations.getFromDate())%>
                                </td>
                                <td>
                                    <%=dateFormat.format(reservations.getToDate())%>
                                </td>
                                <td>
                                    <button class="btn btn-sm yellow table-group-action-submit yes_request" id="<%=reservations.getId()%>"><i class="fa fa-check"></i>Yes</button>
                                    <button class="btn btn-sm red filter-cancel no_request" id="<%=reservations.getId()%>"><i class="fa fa-times"></i> No</button>
                                </td>


                            </tr>
                            <%
                                    }
                                }
                            %>

                        </tbody>
                    </table>

                </div>

            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>


</div>
<script>
    jQuery(document).ready(
            function() {
                TableAdvanced.init("reservation_request");
            }
    );
</script>
<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/scripts/custom/ui-alert-dialog-api.js"></script>