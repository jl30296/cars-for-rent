
<%@page import="ejb.ReservationTable"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DateFormat"%>

<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div>
                   Canceled Reservations List
                </div>
                <%
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                %>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-hover" id='canceled_requests'>

                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    User
                                </th>
                                 <th>
                                    Client
                                </th>

                                <th>
                                    From
                                </th>

                                <th>
                                    To
                                </th>


                            </tr>
                        </thead>
                        <tbody>

                            <%                                List<ReservationTable> listReservations = (List) request.getAttribute("reservations");
                                int i = 0;
                                if (listReservations != null) {
                                    for (ReservationTable reservations : listReservations) {


                            %>  
                            <tr>
                                <td>
                                    <%=++i%>
                                </td>
                                <td>
                                    <%=reservations.getUserId().getName()%>
                                </td>
                                <td>
                                    <%=reservations.getClientId().getName()%>
                                </td>

                                <td>
                                    <%=dateFormat.format(reservations.getFromDate())%>
                                </td>
                                <td>
                                    <%=dateFormat.format(reservations.getToDate())%>
                                </td>


                            </tr>
                            <%
                                    }
                                }
                            %>

                        </tbody>
                    </table>
                </div>
            </div>


        </div>

<script>
    jQuery(document).ready(
      function(){
          TableAdvanced.init("canceled_requests");
      }      
    );
</script>
 <script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/scripts/custom/ui-alert-dialog-api.js"></script>