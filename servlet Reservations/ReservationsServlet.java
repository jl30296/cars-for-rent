/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ejb.CarTable;
import ejb.CarTableFacade;
import ejb.ClientTable;
import ejb.ClientTableFacade;
import ejb.ReservationTable;
import ejb.ReservationTableFacade;
import ejb.UserTable;
import ejb.UserTableFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Controller;

/**
 *
 * @author admin
 */
public class ReservationsServlet extends HttpServlet {
    @EJB
    private UserTableFacade userTableFacade;
    @EJB
    private ClientTableFacade clientTableFacade;

    @EJB
    private CarTableFacade carTableFacade;
    @EJB
    private ReservationTableFacade reservationTableFacade;
    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        this.request = request;
        this.response = response;

        Controller.invokeMethods(this, request, response);
    }

    public void addCalendarView() {
        try {
            List<ReservationTable> reservations = reservationTableFacade.findAll();
            request.setAttribute("reservations", reservations);
            
            
            List<CarTable> vehicles = carTableFacade.findAll();
            request.setAttribute("vehicles", vehicles);

            List<UserTable> users = userTableFacade.findAll();
            request.setAttribute("users", users);
            
            request.getRequestDispatcher("javacalendar.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addReservationsRequestsView() {
        try {
            List<ReservationTable> reservations = reservationTableFacade.status();
            
            request.setAttribute("reservations", reservations);

            request.getRequestDispatcher("Reservations/ReservationsRequests.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void yesRequestView() {
        try {
            int id = Integer.parseInt(request.getParameter("yesId"));
            ReservationTable rese = reservationTableFacade.find(id);
            request.setAttribute("reservation", rese);
            
            List<ClientTable> clients = clientTableFacade.status();
            request.setAttribute("clients", clients);
            List<CarTable> list = carTableFacade.findAll();
            request.setAttribute("list", list);
            request.getRequestDispatcher("Reservations/AcceptReservation.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void acceptRequest() {
        try (PrintWriter out = response.getWriter()) {
            int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
            int clientId = Integer.parseInt(request.getParameter("client"));
            int reservationId = Integer.parseInt(request.getParameter("reservationId"));
            CarTable vehicle = carTableFacade.find(vehicleId);
            String fromDate = request.getParameter("fromDate");

            String toDate = request.getParameter("toDate");

            SimpleDateFormat smdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fDate = smdf.parse(fromDate);
            Date tDate;
            try {
                tDate = smdf.parse(toDate);

                ReservationTable reservation = reservationTableFacade.find(reservationId);
                ClientTable client = clientTableFacade.find(clientId);
                //

                System.out.println(fDate + "from date");
                System.out.println(reservation.getFromDate() + "from reser date");

                System.out.println(tDate + "to date");
                System.out.println(reservation.getToDate() + "t oreser date");
                if (reservationTableFacade.existingRes(vehicleId, fDate, tDate)) {
                    out.println("sdfsdfsdf");
                    return;
                }
                reservation.setCarId(vehicle);
                reservation.setClientId(client);
                reservationTableFacade.changeReservationStatus(reservation);

                boolean result = reservationTableFacade.editReservation(reservation);
                if (result) {
                    out.println("success");
                } else {
                    out.println("error");
                }
            } catch (ParseException ex) {
                Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addAcceptedRequestListView() {
        try {
            List<ReservationTable> reservations = reservationTableFacade.acceptedRequests();
            request.setAttribute("reservations", reservations);
            request.getRequestDispatcher("Reservations/AcceptedRequestList.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void addCanceledRequestsListView(){
        try {
            List<ReservationTable> reservations=reservationTableFacade.canceledRequests();
            request.setAttribute("reservations", reservations);
            request.getRequestDispatcher("Reservations/CanceledRequestsList.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 public void noRequest() {
        try (PrintWriter out = response.getWriter()) {
            int reservationId = Integer.parseInt(request.getParameter("noId"));
            ReservationTable reservation = reservationTableFacade.find(reservationId);
            boolean result = reservationTableFacade.delete(reservation);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(ReservationsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
