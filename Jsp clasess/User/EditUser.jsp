
<%@page import="java.util.List"%>
<%@page import="ejb.UserTable"%>
<%@page import="ejb.GroupTable"%>


<div class="portlet-body form">
    
    <form role="form" class="form-horizontal">
        <div class="form-body">
              <%
            UserTable user=(UserTable)request.getAttribute("user");
            
            //GroupTable group=(GroupTable)request.getAttribute("group");
        %>
               
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Name</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="name" value="<%=user.getName()%>">
                    </div>
                </div>
            </div>
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Surname</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="surname" value="<%=user.getSurname()%>">
                    </div>
                </div>
            </div>
             <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Username</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" value="<%=user.getUsername()%>" id="username">
                    </div>
                </div>
            </div>
             <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name" >Password</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="password" class="form-control" id="password" value="<%=user.getPassword()%>">
                    </div>
                </div>
            </div>
                  <div class="form-group">
                <label class="col-md-4 control-label">Group</label>
                <div class="col-md-8">
                    <select class="form-control" id="selecti">
                        <%
                            List<GroupTable> listGroups=(List)request.getAttribute("list");
                            if(listGroups != null){
                                for(GroupTable group_list:listGroups){
                                    %>
                                    <option id="group_id" value="<%=group_list.getId()%>"><%=group_list.getName()%></option>
                                    <%
                                }
                            }
                        %>
                    </select>
                </div>
            </div>
            
        </div>
        <div class="form-actions fluid">
            <div class="col-md-offset-4 col-md-8">

                <button type="button" class="btn blue" id="submit_button">Save</button>
            </div>
        </div>
    </form>
</div> <script>
                        var globalId=<%=user.getId()%>
                    </script>
        <script src="incl/User/editUser.js"></script>  
