
<%@page import="ejb.UserTable"%>
<%@page import="java.util.List"%>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i>Users List
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="user_table">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Surname
                        </th>

                        <th>
                            Username
                        </th>
                        <th>
                            Group name
                        </th>
                        <th>
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% List<UserTable> list = (List<UserTable>) request.getAttribute("userList");
                        if (list != null) {
                            int i = 0;
                            for (UserTable user : list) {
                    %>
                    <tr>
                        <td>
                            <%=++i%>
                        </td>
                        <td>
                            <% out.print(user.getName());%>
                        </td>

                        <td>
                            <% out.print(user.getSurname());%>
                        </td>
                        <td>
                            <% out.print(user.getUsername());%>
                        </td>
                        <td>
                            <% out.print(user.getGroupId().getName());%>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger delete_button" id="<%=user.getId()%>" >Delete</button> 
                            <button type="button" class="btn btn-primary modifyButton" id="<%=user.getId()%>">Modify</button>
                        </td>
                        <%}
                                }%>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="incl/User/editUser.js" ></script>
<script>
    jQuery(document).ready(
      function(){
          TableAdvanced.init("user_table");
      }      
    );
</script>
