
<%@page import="ejb.GroupTable"%>
<%@page import="java.util.List"%>

    <form role="form" class="form-horizontal">
        <div class="form-body">


            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Name</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="user_name">
                    </div>
                </div>
            </div>
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Surname</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="user_surname">
                    </div>
                </div>
            </div>
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Username</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="user_username">
                    </div>
                </div>
            </div>
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Password</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="password" class="form-control" id="user_password">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Group</label>
                <div class="col-md-8">
                    <select class="form-control">
                        <option></option>
                        <%
                            List<GroupTable> listGroups = (List) request.getAttribute("list");
                            if (listGroups != null) {
                                for (GroupTable group : listGroups) {
                        %>
                        <option id="group_id" value="<%=group.getId()%>"><%=group.getName()%></option>
                        <%
                                }
                            }
                        %>
                    </select>
                </div>
            </div>





        </div>
        <div class="form-actions fluid">
            <div class="col-md-offset-4 col-md-8">

                <button type="button" class="btn blue" id="submit_button">Save</button>
            </div>
        </div>
    </form>
</div>

<script src="incl/User/addUser.js"></script>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>
