<%@page import="ejb.ClientTable"%>
<%@page import="ejb.CarTable"%>
<%@page import="ejb.ReservationTable"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<%@page import="java.util.List"%>

<div class="portlet-body form">

    <form role="form" class="form-horizontal">
        <div class="form-body">
            <%
                ReservationTable reservation = (ReservationTable) request.getAttribute("reservation");
                
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                System.out.println(dateFormat.format(reservation.getFromDate()));
            %>


            <div class="form-group">
                <label class="col-md-4 control-label" for="inputSuccess" >Description</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="description" disabled value="<%=reservation.getDescription()%>">
                    </div>
                </div>
            </div>
                    
                    
                    <div class="form-group">
                <label class="col-md-4 control-label" for="inputSuccess" >User</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="user" disabled value="<%=reservation.getUserId().getName()%>">
                    </div>
                </div>
            </div>



         <div class="form-group">
                <label class="col-md-4 control-label" for="inputSuccess">From</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="date" class="form-control" id="from_date" disabled value="<%=dateFormat.format(reservation.getFromDate())%>">
                    </div>
                </div>
            </div>
                    
                    
         <div class="form-group">
                <label class="col-md-4 control-label" for="inputSuccess">To</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="date" class="form-control" id="to_date" disabled value="<%=dateFormat.format(reservation.getToDate())%>">
                    </div>
                </div>
            </div>



            <div class="form-group" id="user_group">
                <label class="control-label col-md-4" id="label_group">Vehicle</label>
                <div class="col-md-8">
                    <select class="bs-select form-control" id="vehicle">
                        <option></option>
                        <%                            List<CarTable> list = (List) request.getAttribute("list");
                            if (list != null) {
                                for (CarTable vehicle : list) {
                        %>
                        <option value="<%=vehicle.getId()%>"><%=vehicle.getTypeId().getModelId().getName()+ "--->" + vehicle.getTypeId().getName()+ "-" + vehicle.getRegistration()%></option>
                        <%
                                }
                            }
                        %>

                    </select>
                </div>
            </div>
                        
                        <div class="form-group" id="user_group">
                <label class="control-label col-md-4" id="label_group">Client</label>
                <div class="col-md-8">
                    <select class="bs-select form-control" id="client">
                        <option></option>
                        <%                            List<ClientTable> clients = (List) request.getAttribute("clients");
                            if (list != null) {
                                for (ClientTable client : clients) {
                        %>
                        <option value="<%=client.getId()%>"><%=client.getName()%></option>
                        <%
                                }
                            }
                        %>

                    </select>
                </div>
            </div>

        </div>
        <div class="form-actions fluid">
            <div class="col-md-offset-4 col-md-8">

                <button type="button" class="btn blue" id="submit_button">Save</button>
            </div>
        </div>
    </form>
</div>
<script>
                            var reservationId =<%=reservation.getId()%>;
</script>

<script src="incl/Reservations/js/resevationsRequests.js"></script>
 <script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/scripts/custom/ui-alert-dialog-api.js"></script>