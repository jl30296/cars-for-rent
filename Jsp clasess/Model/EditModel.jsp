
<%@page import="ejb.ModelTable"%>
<%@page import="java.util.List"%>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>
<div class="portlet-body form">
    <%
            ModelTable model=(ModelTable)request.getAttribute("model");
        %>
    <form role="form" class="form-horizontal">
        <div class="form-body">
              
               
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Name</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="modelname" value="<%=model.getName()%>">
                    </div>
                </div>
            </div>
          
        
        </div>
        <div class="form-actions fluid">
            <div class="col-md-offset-4 col-md-8">

                <button type="button" class="btn blue" id="submit_button">Save</button>
            </div>
        </div>
    </form>
                   
</div>
                    <script>
                        var globalId=<%=model.getId()%>
                    </script>
        <script src="incl/Model/editModel.js"></script>              