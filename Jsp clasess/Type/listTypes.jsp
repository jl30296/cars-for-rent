
<%@page import="ejb.TypeTable"%>
<%@page import="java.util.List"%>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i>Types List
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>


            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table" id="types_table">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Model
                        </th>
                        <th>
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% List<TypeTable> list = (List<TypeTable>) request.getAttribute("list");
                        if (list != null) {
                            int i = 0;
                            for (TypeTable type : list) {
                    %>
                    <tr>
                        <td>
                            <%=++i%>
                        </td>
                        <td>
                            <% out.print(type.getName());%>
                        </td>
                        <td>
                            <% out.print(type.getModelId().getName());%>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger delete_button" id="<%=type.getId()%>" >Delete</button> 
                            <button type="button" class="btn btn-primary modifyButton" id="<%=type.getId()%>">Modify</button>
                        </td>
                        <%}
                                }%>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="incl/Type/editType.js"></script>
</form>
</div>
<script>
    jQuery(document).ready(
      function(){
          TableAdvanced.init("types_table");
      }      
    );
</script>