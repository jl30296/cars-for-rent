
<%@page import="ejb.ModelTable"%>
<%@page import="java.util.List"%>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>
<script src="incl/Type/addType.js"></script>
<div class="portlet-body form">
    <form role="form" class="form-horizontal">
        <div class="form-body">
              
               
            <div class="form-group" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Name</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="type_name">
                    </div>
                </div>
            </div>
            
                  <div class="form-group">
                <label class="col-md-4 control-label">Group</label>
                <div class="col-md-8">
                    <select class="form-control" id="selecti">
                        <option></option>
                        <%
                            List<ModelTable> listModels=(List)request.getAttribute("listModels");
                            if(listModels != null){
                                for(ModelTable model:listModels){
                                    %>
                                    <option id="model_id" value="<%=model.getId()%>"><%=model.getName()%></option>
                                    <%
                                }
                            }
                        %>
                    </select>
                </div>
            </div>

          
        
           
         

        </div>
        <div class="form-actions fluid">
            <div class="col-md-offset-4 col-md-8">

                <button type="button" class="btn blue" id="submit_button">Save</button>
            </div>
        </div>
    </form>
</div>
