
<%@page import="java.util.List"%>
<%@page import="ejb.TypeTable"%>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>
<div class="portlet-body form">
    <form role="form" class="form-horizontal">
        <div class="form-body">
              
               
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Name</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="vehiclename">
                    </div>
                </div>
            </div>
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Color</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="color" class="form-control" id="vehiclecolor">
                    </div>
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-4 control-label">Type</label>
                <div class="col-md-8">
                    <select class="form-control" id="selecti">
                        <option></option>
                        <%
                            List<TypeTable> listTypes=(List)request.getAttribute("list");
                            if(listTypes != null){
                                for(TypeTable type:listTypes){
                                    %>
                                    <option id="type_id" value="<%=type.getId()%>"><%=type.getName()%></option>
                                    <%
                                }
                            }
                        %>
                    </select>
                </div>
            </div>
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Registration</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="vehicleregistration">
                    </div>
                </div>
            </div>
          
            <div class="form-group" id="group_name" >
                <label class="col-md-4 control-label" for="inputSuccess" id="label_name">Price</label>
                <div class="col-md-8">
                    <div class="input-icon right">
                        <input type="text" class="form-control" id="vehicleprice">
                    </div>
                </div>
            </div>
          

        </div>
        <div class="form-actions fluid">
            <div class="col-md-offset-4 col-md-8">

                <button type="button" class="btn blue" id="submit_button">Save</button>
            </div>
        </div>
    </form>
</div>
<script src="incl/Vehicle/addVehicle.js"></script>