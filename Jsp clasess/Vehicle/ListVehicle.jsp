
<%@page import="ejb.CarTable"%>
<%@page import="java.util.List"%>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i>Vehicles List
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <div class="portlet-body flip-scroll">
        <table class="table table-bordered table-striped table-condensed flip-content" id="vehicle_table">
            <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Type
                    </th>
                    <th>
                        Price
                    </th>
                    <th>
                        Registration
                    </th>
                    <th>
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                <% List<CarTable> list = (List<CarTable>) request.getAttribute("list");
                    if (list != null) {
                        int i = 0;
                        for (CarTable group : list) {
                %>
                <tr>
                    <td>
                        <%=++i%>
                    </td>
                    <td>
                        <% out.print(group.getName());%>
                    </td>
                    <td>
                        <% out.print(group.getTypeId().getName());%>
                    </td>
                    <td>
                        <% out.print(group.getPrice());%>
                    </td>
                    <td>
                        <% out.print(group.getRegistration());%>
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger delete_button" id="<%=group.getId()%>">Delete</button> 
                        <button type="button" class="btn btn-primary modifyButton" id="<%=group.getId()%>">Modify</button>
                    </td>
                    <%}
                                }%>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<script src="incl/Vehicle/editVehicle.js"></script>
</form>
</div>
<script>
    jQuery(document).ready(
      function(){
          TableAdvanced.init("vehicle_table");
      }      
    );
</script>
