
<%@page import="ejb.GroupTable"%>
<%@page import="java.util.List"%>

<script src="template/assets/plugins/bootbox/bootbox.min.js"></script>
<script src="template/assets/plugins/bootbox/bootbox.min_1.js"></script>
<div class="portlet box yellow">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i>Groups List
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>

            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="group_table">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Name
                        </th>

                        <th>
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <% List<GroupTable> list = (List<GroupTable>) request.getAttribute("list");
                        if (list != null) {
                            int i = 0;
                            for (GroupTable group : list) {
                    %>
                    <tr>
                        <td>
                            <%=++i%>
                        </td>
                        <td>
                            <% out.print(group.getName());%>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger delete_button" id="<%=group.getId()%>" >Delete</button> 
                            <button type="button" class="btn btn-primary modifyButton" id="<%=group.getId()%>">Modify</button>
                        </td>
                        <%}
                            }%>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="incl/Group/editGroup.js"></script>
</form>
</div>
<script>
    jQuery(document).ready(
            function () {
                TableAdvanced.init("group_table");
            }
    );
</script>